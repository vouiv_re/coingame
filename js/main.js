"use strict";

let amount = 178;
let coins_type = [10, 5, 2, 1];

function get_change(amount) {
  let result = [0, 0, 0, 0];
  let sum = 0;
  while (sum < amount) {
    if (amount - sum >= coins_type[0]) {
      sum += coins_type[0];
      result[0]++;
    } else if (amount - sum  >= coins_type[1]) {
      sum += coins_type[1];
      result[1]++;
    } else if (amount - sum  >= coins_type[2]) {
      sum += coins_type[2];
      result[2]++;
    } else if (amount - sum  >= coins_type[3]) {
      sum += coins_type[3];
      result[3]++;
    }
  }
  let render = { "bill10" : result[0], "bill5" : result[1], "coin2" : result[2], "coin1" : result[3] };
  return render;
}

console.log(get_change(amount));
